;;; eglot-ccls-semhl.el --- Do semantic highlighting using `ccls'  -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Tobias Pisani
;; Copyright (C) 2018-2020 Fangrui Song
;; Copyright (C) 2022 Akib Azmain Turja.

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <https://www.gnu.org/licenses/>.

;; This file is based on code from https://github.com/MaskRay/emacs-ccls:

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and-or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:

;; Enable semantic highlighting with M-x eglot-ccls-semhl-mode.

;;; Code:

(require 'subr-x)
(require 'eglot)

;; ---------------------------------------------------------------------
;;   Customization
;; ---------------------------------------------------------------------

(defgroup eglot-ccls-semhl nil
  "ccls semantic highlight."
  :group 'tools
  :group 'ccls)

(defface eglot-ccls-semhl-skipped-range-face
  '((t :inherit shadow))
  "The face used to mark skipped ranges."
  :group 'eglot-ccls-semhl)

(defvar eglot-ccls-semhl-face-function 'eglot-ccls--semhl-default-face
  "Function used to determine the face of a symbol in semantic highlight.")

(defface eglot-ccls-semhl-global-variable-face
  '((t :weight extra-bold))
  "The additional face for global variables."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-local-face
  '((t :weight normal))
  "The additional face for local entities."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-local-function-face
  '((t :inherit eglot-ccls-semhl-local-face))
  "The additional face for local functions."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-member-face
  '((t :slant italic))
  "The extra face applied to member functions/variables."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-static-face
  '((t :weight bold))
  "The additional face for variables with static storage."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-static-field-face
  '((t :inherit eglot-ccls-semhl-static-face))
  "The additional face for static member variables."
  :group 'eglot-ccls-semhl)

(defface eglot-ccls-semhl-static-method-face
  '((t :inherit eglot-ccls-semhl-static-face))
  "The additional face for static member functions."
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-function-faces [font-lock-function-name-face]
  "Faces for functions."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-macro-faces [font-lock-variable-name-face]
  "Faces for macros."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-namespace-faces [font-lock-constant-face]
  "Faces for namespaces."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-parameter-faces [font-lock-variable-name-face]
  "Faces for parameters."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-type-faces [font-lock-type-face]
  "Faces used to mark types."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-variable-faces [font-lock-variable-name-face]
  "Faces for variables."
  :type '(repeat face)
  :group 'eglot-ccls-semhl)

;; Default colors used by `eglot-ccls-semhl-use-default-rainbow-highlight'
(defcustom eglot-ccls-semhl-function-colors
  '("#e5b124" "#927754" "#eb992c" "#e2bf8f" "#d67c17"
    "#88651e" "#e4b953" "#a36526" "#b28927" "#d69855")
  "Default colors for `eglot-ccls-semhl-function-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-macro-colors
  '("#e79528" "#c5373d" "#e8a272" "#d84f2b" "#a67245"
    "#e27a33" "#9b4a31" "#b66a1e" "#e27a71" "#cf6d49")
  "Default colors for `eglot-ccls-semhl-macro-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-namespace-colors
  '("#429921" "#58c1a4" "#5ec648" "#36815b" "#83c65d"
    "#417b2f" "#43cc71" "#7eb769" "#58bf89" "#3e9f4a")
  "Default colors for `eglot-ccls-semhl-namespace-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-parameter-colors
  '("#429921" "#58c1a4" "#5ec648" "#36815b" "#83c65d"
    "#417b2f" "#43cc71" "#7eb769" "#58bf89" "#3e9f4a")
  "Default colors for `eglot-ccls-semhl-parameter-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-type-colors
  '("#e1afc3" "#d533bb" "#9b677f" "#e350b6" "#a04360"
    "#dd82bc" "#de3864" "#ad3f87" "#dd7a90" "#e0438a")
  "Default colors for `eglot-ccls-semhl-type-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-variable-colors
  eglot-ccls-semhl-parameter-colors
  "Default colors for `eglot-ccls-semhl-variable-faces'."
  :type '(repeat color)
  :group 'eglot-ccls-semhl)

(defcustom eglot-ccls-semhl-enable-skipped-ranges t
  "Enable skipped ranges.
Regions that are disabled by preprocessors will be displayed in shadow."
  :group 'eglot-ccls-semhl
  :type 'boolean)

(defcustom eglot-ccls-semhl-method nil
  "The method used to draw semantic highlight.
overlays are more accurate than font-lock, but slower.
If nil, disable semantic highlight."
  :group 'eglot-ccls-semhl
  :type '(radio
          (const nil)
          (const :tag "Overlay" overlay)
          (const :tag "Font-Lock" font-lock)))

;; ---------------------------------------------------------------------
;;   Semantic highlight
;; ---------------------------------------------------------------------

(defvar eglot-ccls-semhl-mode) ; Forward declare

(defvar-local eglot-ccls--semhl-skipped-ranges-overlays nil
  "Skipped ranges overlays.")
(defvar-local eglot-ccls--semhl-overlays nil
  "Semantic highlight overlays.")

(defun eglot-ccls--semhl-clear ()
  "."
  (pcase eglot-ccls-semhl-method
    ('overlay
     (while eglot-ccls--semhl-overlays
       (delete-overlay (pop eglot-ccls--semhl-overlays))))
    ('font-lock
     (font-lock-ensure))))

(defun eglot-ccls--semhl-default-face (symbol)
  "Get semantic highlight face of SYMBOL."
  ;; https://github.com/ccls-project/ccls/blob/master/src/symbol.h
  (cl-destructuring-bind (&key id parent-kind kind storage
                               &allow-other-keys)
      symbol
    (let* ((fn0 (lambda (faces lo0 hi0)
                  (let* ((n (length faces))
                         (lo (/ (* lo0 n) 1000))
                         (hi (/ (* hi0 n) 1000))
                         (idx (max 0 (if (= lo hi)
                                         (1- hi)
                                       (+ lo (% id (- hi lo)))))))
                    (elt faces idx))))
           (fn (lambda (faces) (elt faces (% id (length faces))))))
      ;; ccls/src/indexer.h ClangSymbolKind
      ;; clang/Index/IndexSymbol.h clang::index::SymbolKind
      (pcase kind
        ;; Functions
        (6 `(,(funcall fn0 eglot-ccls-semhl-function-faces 0 800)
             eglot-ccls-semhl-member-face)) ; Method
        (9 `(,(funcall fn0 eglot-ccls-semhl-function-faces 800 1000)
             eglot-ccls-semhl-member-face)) ; Constructor
        (12 `(,(funcall fn0 eglot-ccls-semhl-function-faces 0 1000)
              ,@(when (= storage 2)
                  '(eglot-ccls-semhl-local-function-face)))) ; Function
        (254 `(,(funcall fn0 eglot-ccls-semhl-function-faces 0 1000)
               eglot-ccls-semhl-static-method-face)) ; StaticMethod

        ;; Types
        (3
         (funcall fn0 eglot-ccls-semhl-namespace-faces 0 1000)) ; Namespace
        ((or 5 23)
         (funcall fn0 eglot-ccls-semhl-type-faces 0 800)) ; Struct, Class
        (10 (funcall fn0 eglot-ccls-semhl-type-faces 800 1000)) ; Enum
        (26
         (funcall fn0 eglot-ccls-semhl-type-faces 0 1000)) ; TypeParameter
        (252
         `(,(funcall fn0 eglot-ccls-semhl-type-faces 0 1000))) ; TypeAlias

        ;; Variables
        (13 `(,(funcall fn0 eglot-ccls-semhl-variable-faces 0 1000)
              ,@(when (member parent-kind '(1 3))
                  '(eglot-ccls-semhl-global-variable-face))
              ,@(when (= storage 2)
                  '(eglot-ccls-semhl-static-face)))) ; Variable
        (253
         (funcall fn0 eglot-ccls-semhl-parameter-faces 0 1000)) ; Parameter
        (255 (funcall fn0 eglot-ccls-semhl-macro-faces 0 1000)) ; Macro
        (8 `(,(funcall fn0 eglot-ccls-semhl-variable-faces 0 1000)
             ,(if (= storage 2)
                  'eglot-ccls-semhl-static-field-face
                'eglot-ccls-semhl-member-face
                ))) ; Field
        (22 `(,(funcall fn0 eglot-ccls-semhl-variable-faces 0 1000)
              eglot-ccls-semhl-member-face)) ; EnumMember

        (_ (funcall fn eglot-ccls-semhl-variable-faces))))))

(cl-defmethod eglot-handle-notification
  (_server (_method (eql $ccls/publishSemanticHighlight)) &key uri symbols)
  "Publish semantic highlight information according to URI and SYMBOLS."
  (when (and eglot-ccls-semhl-method
             uri
             symbols)
    (when-let* ((file (eglot--uri-to-path uri))
                (buffer (find-buffer-visiting file))
                (enabled-p (buffer-local-value 'eglot-ccls-semhl-mode
                                               buffer)))
      (with-current-buffer buffer
        (with-silent-modifications
          (eglot-ccls--semhl-clear)
          (let (overlays)
            (seq-doseq (symbol symbols)
              (when-let ((face (funcall eglot-ccls-semhl-face-function
                                        symbol)))
                (seq-doseq (range (plist-get symbol :ranges))
                  (cl-destructuring-bind (&key L R &allow-other-keys) range
                    (push (list (1+ L) (1+ R) face) overlays)))))
            ;; The server guarantees the ranges are non-overlapping.
            (setq overlays (sort overlays (lambda (x y) (< (car x)
                                                           (car y)))))
            (pcase eglot-ccls-semhl-method
              ('font-lock
               (seq-doseq (x overlays)
                 (set-text-properties (car x) (cadr x)
                                      `(fontified t face ,(caddr x)
                                                  font-lock-face
                                                  ,(caddr x)))))
              ('overlay
               (seq-doseq (x overlays)
                 (let ((ov (make-overlay (car x) (cadr x))))
                   (overlay-put ov 'face (caddr x))
                   (overlay-put ov 'eglot-ccls-semhl t)
                   (push ov eglot-ccls--semhl-overlays)))))))))))

(defmacro eglot-ccls-semhl-use-default-rainbow-highlight ()
  "Use default rainbow semantic highlight theme."
  `(progn
     ,@(cl-loop
        for kind in '("function" "macro" "namespace" "parameter" "type"
                      "variable")
        append
        (let ((colors (intern (format "eglot-ccls-semhl-%s-colors" kind))))
          (append
           (seq-map-indexed
            (lambda (color i)
              `(defface (intern (format "eglot-ccls-semhl-%s-face-%S" ,kind
                                       ,i))
                '((t :foreground ,color)) "." :group 'eglot-ccls-semhl))
            (symbol-value colors))
           (list
            `(setq ,(intern (format "eglot-ccls-semhl-%s-faces" kind))
                   (apply #'vector
                          (cl-loop for i below (length ,colors) collect
                                   (intern
                                    (format "eglot-ccls-semhl-%s-face-%S"
                                            ,kind i)))))))))))

;; ---------------------------------------------------------------------
;;   Skipped ranges
;; ---------------------------------------------------------------------

(defun eglot-ccls--semhl-clear-skipped-ranges ()
  "Clean up overlays."
  (while eglot-ccls--semhl-skipped-ranges-overlays
    (delete-overlay (pop eglot-ccls--semhl-skipped-ranges-overlays))))

(cl-defmethod eglot-handle-notification
  (_server (_method (eql $ccls/publishSkippedRanges))
           &key uri skippedRanges)
  "Put overlays on inactive regions according to URI and SKIPPEDRANGES."
  (when (and eglot-ccls-semhl-enable-skipped-ranges
             uri
             skippedRanges)
    (let ((file (eglot--uri-to-path uri)))
      (when-let* ((buffer (find-buffer-visiting file))
                  (enabled-p (buffer-local-value 'eglot-ccls-semhl-mode
                                                 buffer)))
        (with-current-buffer buffer
          (with-silent-modifications
            (eglot-ccls--semhl-clear-skipped-ranges)
            (overlay-recenter (point-max))
            (seq-doseq (range skippedRanges)
              (let ((ov (make-overlay (eglot--lsp-position-to-point
                                       (plist-get range :start))
                                      (eglot--lsp-position-to-point
                                       (plist-get range :end))
                                      buffer)))
                (overlay-put ov 'face 'eglot-ccls-semhl-skipped-range-face)
                (overlay-put ov 'eglot-ccls-semhl-inactive t)
                (push ov eglot-ccls--semhl-skipped-ranges-overlays)))))))))

;; ---------------------------------------------------------------------
;;   Modes
;; ---------------------------------------------------------------------

;;;###autoload
(define-minor-mode eglot-ccls-semhl-mode
  "Do semantic highlighting."
  nil nil nil
  (unless eglot-ccls-semhl-mode
    (eglot-ccls--semhl-clear)
    (eglot-ccls--semhl-clear-skipped-ranges)))

(provide 'eglot-ccls-semhl)
;;; eglot-ccls-semhl.el ends here
